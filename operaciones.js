/**
 * @author Miguel Reina
 * @copyright Manticore.Labs
 */

var operaciones = {
  
    sumar: (a,b)=>{
        return a+b
    },

    restar: (a,b)=>{
        return a-b
    },

    multiplicar: (a,b)=>{
        return a*b
    },

    dividir: (a,b)=>{
        return a/b
    },

    modulo: (a,b)=>{
        return a%b
    }
}

json1 = {
    b: {
        a: 1
    }
}

json2 = {
    c: {
        b: 1
    }
}

var a = `La suma entre dos numbers es: ` + operaciones.sumar(1,2)
console.log(a)

var a = `La suma entre dos strings es: ` + operaciones.sumar('Hola ', 'mundo.')
console.log(a)

var a = `La suma entre dos null es: ` + operaciones.sumar(null, null)
console.log(a)

var a = `La suma entre dos undefined es: ` + operaciones.sumar()
console.log(a)

var a = `La suma entre dos boolean es: ` + operaciones.sumar(true, false)
console.log(a)

var a = `La suma entre dos JSON es: ` + operaciones.sumar(json1, json2)
console.log(a)

var a = `La suma entre numbers & strings es: ` + operaciones.sumar(96, 'Hola mundo')
console.log(a)

var a = `La suma entre numbers & null es: ` + operaciones.sumar(96, null)
console.log(a)

var a = `La suma entre numbers & undefined es: ` + operaciones.sumar(25)
console.log(a)

var a = `La suma entre numbers & boolean es: ` + operaciones.sumar(96, true)
console.log(a)

var a = `La suma entre numbers & JSON es: ` + operaciones.sumar(96, json1)
console.log(a)
 






var a = `La resta entre dos numbers es: ` + operaciones.restar(1,2)
console.log(a)

var a = `La resta entre dos strings es: ` + operaciones.restar('Hola ', 'mundo.')
console.log(a)

var a = `La resta entre dos null es: ` + operaciones.restar(null, null)
console.log(a)

var a = `La resta entre dos undefined es: ` + operaciones.restar()
console.log(a)

var a = `La resta entre dos boolean es: ` + operaciones.restar(true, false)
console.log(a)

var a = `La resta entre dos JSON es: ` + operaciones.restar(json1, json2)
console.log(a)

var a = `La resta entre numbers & strings es: ` + operaciones.restar(96, 'Hola mundo')
console.log(a)

var a = `La resta entre numbers & null es: ` + operaciones.restar(96, null)
console.log(a)

var a = `La resta entre numbers & undefined es: ` + operaciones.restar(25)
console.log(a)

var a = `La resta entre numbers & boolean es: ` + operaciones.restar(96, true)
console.log(a)

var a = `La resta entre numbers & JSON es: ` + operaciones.restar(96, json1)
console.log(a)
 


 


var a = `La multiplicacion entre dos numbers es: ` + operaciones.multiplicar(1,2)
console.log(a)

var a = `La multiplicacion entre dos strings es: ` + operaciones.multiplicar('Hola ', 'mundo.')
console.log(a)

var a = `La multiplicacion entre dos null es: ` + operaciones.multiplicar(null, null)
console.log(a)

var a = `La multiplicacion entre dos undefined es: ` + operaciones.multiplicar()
console.log(a)

var a = `La multiplicacion entre dos boolean es: ` + operaciones.multiplicar(true, false)
console.log(a)

var a = `La multiplicacion entre dos JSON es: ` + operaciones.multiplicar(json1, json2)
console.log(a)

var a = `La multiplicacion entre numbers & strings es: ` + operaciones.multiplicar(96, 'Hola mundo')
console.log(a)

var a = `La multiplicacion entre numbers & null es: ` + operaciones.multiplicar(96, null)
console.log(a)

var a = `La multiplicacion entre numbers & undefined es: ` + operaciones.multiplicar(25)
console.log(a)

var a = `La multiplicacion entre numbers & boolean es: ` + operaciones.multiplicar(96, true)
console.log(a)

var a = `La multiplicacion entre numbers & JSON es: ` + operaciones.multiplicar(96, json1)
console.log(a)






var a = `La division entre dos numbers es: ` + operaciones.dividir(1,2)
console.log(a)

var a = `La division entre dos strings es: ` + operaciones.dividir('Hola ', 'mundo.')
console.log(a)

var a = `La division entre dos null es: ` + operaciones.dividir(null, null)
console.log(a)

var a = `La division entre dos undefined es: ` + operaciones.dividir()
console.log(a)

var a = `La division entre dos boolean es: ` + operaciones.dividir(true, false)
console.log(a)

var a = `La division entre dos JSON es: ` + operaciones.dividir(json1, json2)
console.log(a)

var a = `La division entre numbers & strings es: ` + operaciones.dividir(96, 'Hola mundo')
console.log(a)

var a = `La division entre numbers & null es: ` + operaciones.dividir(96, null)
console.log(a)

var a = `La division entre numbers & undefined es: ` + operaciones.dividir(25)
console.log(a)

var a = `La division entre numbers & boolean es: ` + operaciones.dividir(96, true)
console.log(a)

var a = `La division entre numbers & JSON es: ` + operaciones.dividir(96, json1)
console.log(a)

 




var a = `El modulo entre dos numbers es: ` + operaciones.modulo(1,2)
console.log(a)

var a = `El modulo entre dos strings es: ` + operaciones.modulo('Hola ', 'mundo.')
console.log(a)

var a = `El modulo entre dos null es: ` + operaciones.modulo(null, null)
console.log(a)

var a = `El modulo entre dos undefined es: ` + operaciones.modulo()
console.log(a)

var a = `El modulo entre dos boolean es: ` + operaciones.modulo(true, false)
console.log(a)

var a = `El modulo entre dos JSON es: ` + operaciones.modulo(json1, json2)
console.log(a)

var a = `El modulo entre numbers & strings es: ` + operaciones.modulo(96, 'Hola mundo')
console.log(a)

var a = `El modulo entre numbers & null es: ` + operaciones.modulo(96, null)
console.log(a)

var a = `El modulo entre numbers & undefined es: ` + operaciones.modulo(25)
console.log(a)

var a = `El modulo entre numbers & boolean es: ` + operaciones.modulo(96, true)
console.log(a)

var a = `El modulo entre numbers & JSON es: ` + operaciones.modulo(96, json1)
console.log(a) 