## Operaciones entre los tipos de datos que existen en JavaScript

#### Tipos de datos.

Los tipos de datos que existen en JavaScript son:

* Numbers
* Boolean 
* String
* JSON
* null
* Undefined

#### Capturas de pantallas

***Suma***

![image](/uploads/52a5c5a43cedf111490a19bfa28ae1d0/image.png)

***Resta***

![image](/uploads/8e14d011371b95e123eaba3da7dcae17/image.png)

***Multiplicacion***

![image](/uploads/926f2ea90b603f4613ba529fd9dd6841/image.png)

***Division***

![image](/uploads/2b2a01fd367a8dce01f9755d551180b4/image.png)

***Modulo***

![image](/uploads/09e4cfbbc392e5e9b63fb2721cf86114/image.png)



### Documentacion

Codigos usados para levantar el servidor para la documentacion

![image](/uploads/ee219af127fde6655063674f6f8ce90f/image.png)